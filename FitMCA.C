// Written by David Attié (2007) modified in (2013-Jan-30)
// ---------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------
// Macro to plot and fit a MCA spectrum
// ---------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------
// root []
// root [] .L FitMCA.C
// root [] ReadMCA("FitMCA.mca")
// root [] FitMCA(histo, 65., 110.)
// root [] Fit(histo, 65., 110., 1, 2)
// ---------------------------------------------------------------------------------

#include <iostream>
#include <fstream>
#include <vector>
#include "TString.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TH1.h"
#include "TF1.h"

#define DEBUG 0

using namespace std;

// ---------------------------------------------------------------------------------
// Math Functions
// ---------------------------------------------------------------------------------

// Linear background function
Double_t background(Double_t *x, Double_t *par)
{
	if (par[1] > 0) cout << "!!! Bruit croissant !!!";
	return par[0] + par[1]*x[0];
}

// Gaussian function
Double_t fgauss(Double_t *x, Double_t *par)
{
	Double_t arg = 0;
	if (par[2] != 0) arg = (x[0] - par[1])/par[2];

	Double_t fitval = par[0]*TMath::Exp(-0.5*arg*arg);
	return fitval;
}

// Sum of background and peak function
Double_t fitFunction(Double_t *x, Double_t *par)
{
	return background(x,par) + fgauss(x,&par[2]);
}

// Gaussian function
Double_t fdgauss(Double_t *x, Double_t *par)
{
	Double_t arg1 = 0;
	Double_t arg2 = 0;
	if (par[2] != 0) {
		arg1 = (x[0] - par[1])/par[2];
		par[5] = par[2];
		arg2 = (x[1] - par[4])/par[5];
	}
	
	Double_t fitval = par[0]*TMath::Exp(-0.5*arg1*arg1)+par[3]*TMath::Exp(-0.5*arg2*arg2);
	return fitval;
}

// ---------------------------------------------------------------------------------
// Reading procedure
// ---------------------------------------------------------------------------------

void ReadMCA(TString fname)
{

	ifstream MyFile( fname );

	Int_t nlines = 0; 
	Int_t counter = 0;

	string Line;

	if ( MyFile ) 			// ce test échoue si le fichier n'est pas ouvert
	{
		while ( MyFile ) 
		{ 
			getline(MyFile, Line);
			if (Line != "<<END>>")
			{	
				//cout << Line << endl;
				if (Line == "<<DATA>>")
				{
					counter = nlines+1;
				}
         	++nlines; 
			}
		} 
	}
	else
	{
		cout << "File " << fname << " do not exist !\n\n Exit program !!\n\n" ;
		exit(EXIT_FAILURE);
	}
	 
	MyFile.close();

 
	ifstream MyFile2( fname );
	string inBuf;
	Int_t lineNumber = 0;
	Int_t ncolc = counter+1;
	Int_t ncol  = nlines-ncolc;

    // const NBINS = nlines;
	const int NBINS = ncol;
	Float_t x_first = 1;
	Float_t x_last  = NBINS;
	
	Int_t datax[NBINS];
	Int_t data[NBINS];
	string str_buf;
	Int_t i = 0;

	while (getline(MyFile2, inBuf) && (inBuf != "<<END>>"))
	{
		++lineNumber;
		//
		// I'll print a message in place of the skipped line. You
		// could simply do nothing (i.e. delete the else{} stuff)
		//
		if (lineNumber <= counter)
		{
			if (DEBUG) cout << lineNumber << " skipped : " << inBuf << endl;
		} 
		else 
		{
	 		if (DEBUG) cout << lineNumber << ": " << inBuf << endl;
			datax[i] = i;
			data[i] = atoi(inBuf.c_str());
			i++;
		}
	}

	MyFile2.close();

	cout << "nlines : " << ncol << endl; 
	
	/////////////////

	TCanvas *Fit_result = new TCanvas("Fit_result", "FitMCA by David Attie (2007)", 680, 440);
	Fit_result->SetBorderSize(0);
	Fit_result->SetFrameFillColor(0);
	Fit_result->SetFrameBorderMode(0);
	Fit_result->SetHighLightColor(2);
	Fit_result->SetFillColor(10);
	Fit_result->SetBorderMode(0);
	Fit_result->SetBorderSize(0);
	Fit_result->SetFrameFillColor(10);
	Fit_result->SetFrameFillStyle(0);	

	TH1F * histo = new TH1F("histo", "", ncol, x_first, x_last);
	
	histo->GetYaxis()->SetTitleOffset(1.3);
	histo->SetTitle(fname);
	histo->SetXTitle("Channel");
	histo->SetYTitle("Counts");
	histo->GetXaxis()->SetLabelFont(42);
	histo->GetXaxis()->SetTitleFont(42);
	histo->GetYaxis()->SetLabelFont(42);
	histo->GetYaxis()->SetTitleFont(42);
	histo->Draw();

	MyFile.open(fname.Data());
  
	for (Int_t i=0; i<ncol; i++)
	{
		histo->SetBinContent(i, data[i]);		
	}
  
	histo->SetDirectory(0);
	histo->SetStats(0);
	histo->Draw("same");

}

// ---------------------------------------------------------------------------------
// Fitting procedures
// ---------------------------------------------------------------------------------

void FitGauss(TH1F * histog, Float_t x1, Float_t x2)
{
	// create a TF1 with the range from x1 to x2 and 5 parameters
	TF1 *fitFcn = new TF1("fitFcn",fgauss,x1,x2,3);
	fitFcn->SetNpx(500);
	fitFcn->SetLineColor(kRed);

	Double_t a = 1, b = 1;
	Double_t nBin = (Double_t) histog->GetNbinsX();
	Double_t mean = (Double_t) histog->GetMean();
	Double_t rms  = (Double_t) histog->GetRMS();
	Double_t max  = (Double_t) histog->GetMaximum();
	
	fitFcn->SetParameters( max, mean, rms );
	fitFcn->SetParNames( "Constant", "Mean Value", "Sigma" );

	histog->Fit(fitFcn,"","",x1,x2);

	Double_t par[3];	
	// writes the fit results into the par array
	fitFcn->GetParameters(par);
  
	// draw the legend
	//TLegend *legend=new TLegend(0.6,0.65,0.88,0.85);
	TLegend *legend=new TLegend(0.126,0.681,0.407,0.881);
	legend->SetTextFont(42);
	legend->SetFillColor(0);
	legend->SetBorderSize(0);
	legend->SetTextSize(0.03);
	legend->AddEntry(histog,"Data","lpe");
	legend->AddEntry(fitFcn,"Gauss Fit","l");
	legend->Draw("same");
}

void fRange(TH1F * histor, Int_t x1, Int_t x2)
{
	histor->GetXaxis()->SetRange(x1,x2);
	histor->Draw("same");
}

void FitMCA(TH1F * histofit, Float_t x1, Float_t x2)
{
	// create a TF1 with the range from x1 to x2 and 5 parameters
	TF1 *fitFcn = new TF1("fitFcn",fitFunction,x1,x2,5);
	fitFcn->SetNpx(500);
	fitFcn->SetLineColor(kRed);
	
	Double_t a = 1, b = 1;
	Double_t nBin = (Double_t) histofit->GetNbinsX();
	Double_t mean = (Double_t) histofit->GetMean();
	Double_t rms  = (Double_t) histofit->GetRMS();
	Double_t max  = (Double_t) histofit->GetMaximum();
	
	fitFcn->SetParameters( b, a, max, mean, rms );
	fitFcn->SetParLimits(0,0., 5000.);
	fitFcn->SetParLimits(1,-20., 0.);
	fitFcn->SetParNames( "b", "a", "Constant", "Mean Value", "Sigma" );
	
	//histofit->Fit("fitFcn","0","",x1,x2);
	//	histofit->Fit("fitFcn","R0");
	
	histofit->Fit(fitFcn,"","",x1,x2);
		
	// improve the picture:
	TF1 *backFcn = new TF1("backFcn",background,x1,x2,2);
	//  backFcn->SetRange(x1,x2);
	backFcn->SetLineColor(kMagenta);
	backFcn->SetLineWidth(1);
	TF1 *signalFcn = new TF1("signalFcn",fgauss,x1,x2,3);
	//  signalFcn->SetRange(x1,x2);
	signalFcn->SetLineColor(kBlue);
	signalFcn->SetLineWidth(1);
	signalFcn->SetNpx(500);
	Double_t par[5];


	
	
	// writes the fit results into the par array
	fitFcn->GetParameters(par);
	
	backFcn->SetParameters(par);
	backFcn->Draw("same");
	signalFcn->SetParameters(&par[2]);
	signalFcn->Draw("same"); 	 
	
	// draw the legend
	//TLegend *legend=new TLegend(0.6,0.65,0.88,0.85);
	TLegend *legend=new TLegend(0.126,0.681,0.407,0.881);
	legend->SetTextFont(42);
	legend->SetFillColor(0);
	legend->SetBorderSize(0);
	legend->SetTextSize(0.03);
	legend->AddEntry(histofit,"Data","lpe");
	legend->AddEntry(backFcn,"Background fit","l");
	legend->AddEntry(signalFcn,"Gauss fit","l");
	legend->AddEntry(fitFcn,"Global Fit","l");
	// legend->Draw("same");
	
	Float_t fw, sigma, chi;
	
	chi   = (fitFcn->GetChisquare())/(fitFcn->GetNDF());
	sigma = fitFcn->GetParameter(4)/fitFcn->GetParameter(3) *100;
	fw    = sigma * 2.35;
	
	cout << "" << endl;
	cout << "------------------" << endl;
	cout << "Sigma    = " << sigma << " %" << endl;
	cout << "FWHM     = " << fw << " %" << endl;
	cout << "Chi2/NDF = " << chi << endl;
	cout << "------------------" << endl;
	
}


void Fit(TH1F * histo, Float_t xlow, Float_t xup, Int_t linfit, Int_t kind)
{
	// 1 == 1 gaussian function + 1 linear background
	// 2 == 2 gaussian functions (position 65/59; ratio=1/5) + 1 linear background
	// 3 == 2 gaussian functions (position 65/59) + 1 linear background
	// 4 == 2 gaussian functions + 1 linear background

	TString singleline  = "[0]*exp(-0.5*((x-[1])/[2])^2)+[3]+[4]*x";
	TString doubleline  = "[0]*exp(-0.5*((x-[1])/[2])^2)+[0]*(1./5)*exp(-0.5*((x-[1]*65/59)/[2])^2)+[3]+[4]*x";
	TString doubleline2 = "[0]*exp(-0.5*((x-[1])/[2])^2)+[0]*[5]*exp(-0.5*((x-[1]*65/59)/[2])^2)+[3]+[4]*x";
	TString doubleline3 = "[0]*exp(-0.5*((x-[1])/[2])^2)+[0]*[5]*exp(-0.5*((x-[1]*[6])/[2])^2)+[3]+[4]*x";

	TF1 * myfit;
	
	if (kind == 1) {myfit = new TF1("singleline",  singleline,  xlow, xup);}
	if (kind == 2) {myfit = new TF1("doubleline",  doubleline,  xlow, xup);}
	if (kind == 3) {myfit = new TF1("doubleline2", doubleline2, xlow, xup);}
	if (kind == 4) {myfit = new TF1("doubleline3", doubleline3, xlow, xup);}
	
	myfit->SetLineColor(kBlack);
		
	myfit->SetParameter(0,histo->GetMaximum());
	myfit->SetParameter(1,histo->GetMean());
	myfit->SetParameter(2,histo->GetRMS());
	
	if (linfit == 0) {
		myfit->FixParameter(3,0);
		myfit->FixParameter(4,0);
	}
	
	if ((kind == 3) || (kind == 4)) {myfit->SetParameter(5,0.11);}
	// myfit->FixParameter(5,0.08);}
	
	if (kind == 4) {myfit->SetParameter(6,1.1);}
	
	histo->Fit(myfit,"","",xlow,xup);
	
	Float_t fw, sigma, chi, mean;
	
	chi   = (myfit->GetChisquare())/(myfit->GetNDF());
	mean   = myfit->GetParameter(1);
	sigma = myfit->GetParameter(2)/myfit->GetParameter(1) *100;
	fw    = sigma * 2.35;
	
	cout << "" << endl;
	cout << "------------------" << endl;
	cout << "Sigma    = " << sigma << " %" << endl;
	cout << "FWHM     = " << fw << " %" << endl;
	cout << "Chi2/NDF = " << chi << endl;
	cout << "Mean     = " << mean << endl;
	cout << "------------------" << endl;
	
	TString gaus1 = "[0]*exp(-0.5*((x-[1])/[2])^2)";
	TF1 * gaus1f = new TF1("gauss1",gaus1,xlow,xup);
	gaus1f->SetLineColor(kMagenta);
	gaus1f->SetLineWidth(1);
	
	TString gaus2 = "[0]*exp(-0.5*((x-[1])/[2])^2)";
	TF1 * gaus2f = new TF1("gauss2",gaus2,xlow,xup);
	gaus2f->SetLineColor(kBlue);
	gaus2f->SetLineWidth(1);
	
	TString lin = "[0]+[1]*x";
	TF1 * linf = new TF1("linear1",lin,xlow,xup);
	linf->SetLineColor(kGreen);
	linf->SetLineWidth(1);
	
	gaus1f->SetParameter(0,myfit->GetParameter(0));
	gaus1f->SetParameter(1,myfit->GetParameter(1));
	gaus1f->SetParameter(2,myfit->GetParameter(2));
	
	cout<<""<<endl;
	
	Float_t height = myfit->GetParameter(0)*(1./9.);
	
	if((kind == 3) || (kind ==4)) {
		cout<<myfit->GetParameter(0)<<"  "<<1/myfit->GetParameter(5)<<endl;
		height = (myfit->GetParameter(0)) * (myfit->GetParameter(5));
	}
	
	mean = myfit->GetParameter(1) * (65./59.);
	
	if (kind == 4) {mean = (myfit->GetParameter(1)) * (myfit->GetParameter(6));}
	
	Float_t sig = myfit->GetParameter(2);
	gaus2f->SetParameter(0,height);
	gaus2f->SetParameter(1,mean);
	gaus2f->SetParameter(2,sig);
	
	linf->SetParameter(0,myfit->GetParameter(3));
	linf->SetParameter(1,myfit->GetParameter(4));
	
	gaus1f->Draw("same");
	gaus2f->Draw("same");
	linf->Draw("same");
	
	// draw the legend
	TLegend *legend=new TLegend(0.126,0.681,0.407,0.881);
	legend->SetTextFont(42);
	legend->SetFillColor(0);
	legend->SetBorderSize(0);
	legend->SetTextSize(0.03);
	legend->AddEntry(histo,"Data","l");
	legend->AddEntry(linf,"Linear background","l");
	legend->AddEntry(gaus1f,"Gauss 1","l");
	legend->AddEntry(gaus2f,"Gauss 2","l");
	legend->AddEntry(myfit,"Global fit","l");
	legend->Draw("same");
	
}