// Written by Marion LEHUARUX (2018)
// ---------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------
// Graph Gain et resol
// ---------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------
//comment on s'en sert
// ---------------------------------------------------------------------------------
#include <iostream>
#include <fstream>
#include <vector>
#include "TString.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TLegend.h"
#include "TH1.h"
#include "TF1.h"

using namespace std;

void PlotGain()
{
   Double_t a, b;

   TCanvas *c1 = new TCanvas("c1","Gain du MICROMEGAS",200,10,700,500);
   gPad->SetLogy();
   // Int_t n = 11;
   Int_t x_1[7]  = {400, 405, 410, 415, 420, 450, 455};
   Int_t x_2[5]  = {425, 430, 435, 440, 460};

   Int_t y_1[7]  = {20, 20, 21, 21, 22, 34, 37};
   Int_t y_2[5]  = {51, 58, 67, 75, 240};

   TMultiGraph *mg = new TMultiGraph();
   mg->SetTitle("Gain du MICROMEGAS");

   TGraph *gr1 = new TGraph(7,x_1,y_1);
   TGraph *gr2 = new TGraph(5,x_2,y_2);

   mg->Add(gr1);
   mg->Add(gr2);

   // gr2->Draw("sameP");
   // gr1->GetXaxis()->SetTitle("Tension mesh (V)");
   // gr1->GetYaxis()->SetTitle("Channel MCA");
   gr1->SetMarkerColor(4);
   gr1->SetMarkerStyle(21);
   gr2->SetMarkerColor(2);
   gr2->SetMarkerStyle(21);

   mg->Draw("AP");
   mg->GetXaxis()->SetTitle("Tension mesh (V)");
   mg->GetYaxis()->SetTitle("Channel MCA");
   gPad->Modified();

   // TF1 *fitFcn = new TF1("fitFcn","a*exp(b*x)", 400, 455, 2);
   // fitFcn->SetLineColor(kRed);
   // gr->Fit(fitFcn,"","",400, 455);
   // fitFcn->GetParameters();
   // fitFcn->Draw("same");

   c1->Update();
   return c1;
}

void PlotResol()
{
   TCanvas *c1 = new TCanvas("c1","Resolution du MICROMEGAS",200,10,700,500);
   gPad->SetLogy();
   // Int_t n = 11;
   Double_t x_1[7]  = {400, 405, 410, 415, 420, 450, 455};
   Double_t x_2[5]  = {425, 430, 435, 440, 460};

   Double_t sigma_1[7]  = {0.9, 1.0, 1.2, 1.6, 1.9, 2.6, 2.6};
   Double_t sigma_2[5]  = {3.1, 3.8, 5.1, 5.8, 112};

   Double_t y_1[7];
   Double_t y_2[7];
   int i, j;
   for(i = 0; i < 7; i++) y_1[i] = 2.355*sigma_1[i];
   for(j = 0; j < 5; j++) y_2[j] = 2.355*sigma_2[j];
   
   TMultiGraph *mg = new TMultiGraph();
   mg->SetTitle("Resolution du MICROMEGAS");

   TGraph *gr1 = new TGraph(7,x_1,y_1);
   TGraph *gr2 = new TGraph(5,x_2,y_2);

   mg->Add(gr1);
   mg->Add(gr2);

   gr1->SetMarkerColor(4);
   gr1->SetMarkerStyle(21);
   gr2->SetMarkerColor(2);
   gr2->SetMarkerStyle(21);

   mg->Draw("AP");
   mg->GetXaxis()->SetTitle("Tension mesh (V)");
   mg->GetYaxis()->SetTitle("Resolution");
   gPad->Modified();

   c1->Update();
   return c1;
   return c1;
}

